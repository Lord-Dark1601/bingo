package bingo;

public class Ticket {

	private int[][] carton;

	public Ticket() {
		carton = new int[3][9];
		for (int i = 0; i < carton.length; i++) {
			for (int j = 0; j < carton[0].length; j++) {
				carton[i][j] = 0;
			}
		}
		for (int i = 0; i < carton.length; i++) {
			int count = 0;
			while (count < 5) {
				putNumbers(i);
				count++;
			}
		}
	}

	private void putNumbers(int i) {
		int x = (int) (Math.random() * 9);
		if (validNumber(x)) {
			if (carton[i][x] == 0) {
				makeSwitch(i, x);
			}
		}
	}

	private boolean validNumber(int x) {
		int count = 0;
		for (int i = 0; i < carton.length; i++) {
			if (carton[i][x] != 0) {
				count++;
			}
		}
		if (count < 3) {
			return true;
		} else {
			return false;
		}
	}

	private void makeSwitch(int i, int x) {
		int y;
		switch (x) {
		case 0:
			y = (int) (Math.random() * 9 + 1);
			carton[i][x] = y;
			break;
		case 1:
			y = (int) (Math.random() * 10 + 10);
			carton[i][x] = y;
			break;
		case 2:
			y = (int) (Math.random() * 10 + 20);
			carton[i][x] = y;
			break;
		case 3:
			y = (int) (Math.random() * 10 + 30);
			carton[i][x] = y;
			break;
		case 4:
			y = (int) (Math.random() * 10 + 40);
			carton[i][x] = y;
			break;
		case 5:
			y = (int) (Math.random() * 10 + 50);
			carton[i][x] = y;
			break;
		case 6:
			y = (int) (Math.random() * 10 + 60);
			carton[i][x] = y;
			break;
		case 7:
			y = (int) (Math.random() * 10 + 70);
			carton[i][x] = y;
			break;
		case 8:
			y = (int) (Math.random() * 10 + 80);
			carton[i][x] = y;
			break;
		}
	}

	@Override
	public String toString() {
		String s = "";
		for (int i = 0; i < carton.length; i++) {
			for (int j = 0; j < carton[0].length; j++) {
				s += carton[i][j] + " ";
			}
			s += "\n";
		}
		return s;
	}
}
